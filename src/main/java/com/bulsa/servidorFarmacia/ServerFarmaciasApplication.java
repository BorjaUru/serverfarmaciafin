package com.bulsa.servidorFarmacia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ServerFarmaciasApplication  extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(ServerFarmaciasApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

	private static Class<ServerFarmaciasApplication> applicationClass = ServerFarmaciasApplication.class;
}
