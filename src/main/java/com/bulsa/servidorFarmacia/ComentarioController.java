package com.bulsa.servidorFarmacia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by borja on 19/12/2016.
 */
@RestController
public class ComentarioController {
    @Autowired
    private ComentarioRepository repository;



    @RequestMapping("/comentarios")
    public List<Comentario> getComentarios() {

        List<Comentario> listaComentarios= repository.findAll();
        return listaComentarios;
    }

    @RequestMapping("/add_comentario")
    public void addOpinion(@RequestParam(value = "nombre", defaultValue = "nada") String nombre,
                           @RequestParam(value = "descripcion" , defaultValue = "nada") String descripcion,
                           @RequestParam(value = "nota", defaultValue = "nada") int nota) {

        Comentario c=new Comentario();
        c.setNombre(nombre);
        c.setDescripcion(descripcion);
        c.setNota(nota);
        repository.save(c);
    }
}
