package com.bulsa.servidorFarmacia;


import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by borja on 19/12/2016.
 */
public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    List<Usuario> findAll();
    Usuario findByNombreAndPass(String nombre, String pass);
    Usuario findByEmail(String email);
}