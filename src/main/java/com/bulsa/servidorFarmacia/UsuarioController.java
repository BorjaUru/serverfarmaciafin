package com.bulsa.servidorFarmacia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by borja on 19/12/2016.
 */
@RestController
public class UsuarioController {
    @Autowired
    private UsuarioRepository repository;



    @RequestMapping("/usuario_login")
    public boolean getLogin(String nombre,String pass){
        Usuario usernombre=repository.findByNombreAndPass(nombre,pass);
        if (usernombre == null)
            return false;

        return true;
    }

    @RequestMapping("/add_usuario")
    public boolean addUser(@RequestParam(value = "nombre", defaultValue = "nada") String nombre,
                           @RequestParam(value = "email" , defaultValue = "nada") String email,
                           @RequestParam(value = "pass", defaultValue = "nada") String pass) {
        if(repository.findByEmail(email)!=null){return false;}
        Usuario u = new Usuario();
        u.setNombre(nombre);
        u.setEmail(email);
        u.setPass(pass);

        repository.save(u);
        return true;
    }
}
