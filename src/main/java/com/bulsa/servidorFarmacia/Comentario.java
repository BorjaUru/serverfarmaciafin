package com.bulsa.servidorFarmacia;

import javax.persistence.*;

/**
 * Created by borja on 19/12/2016.
 */
@Entity
@Table(name = "comentarios")
public class Comentario {
    @Id
    @GeneratedValue
    private int id;
    @Column
    private String descripcion;
    @Column
    private int nota;
    @Column
    private String nombre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
}
